﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_02_console
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = "";
            var value = 0.00;
            var total = 0.00;
            ConsoleKeyInfo keyInfo = new ConsoleKeyInfo();

            Console.WriteLine($"Press ESC to stop {Environment.NewLine}Please enter a in number, and press enter");
            do
            {
                input = "";
                keyInfo = Console.ReadKey();
                if (keyInfo.Key == ConsoleKey.Escape)
                {
                    return;
                }
                input += keyInfo.KeyChar.ToString();
                input += Console.ReadLine();

                value = convertToDouble(input);
                if (value == 0)
                {
                    Console.WriteLine($"{input} is not vaild. {Environment.NewLine} Please enter a number value !");
                }
                else
                {
                    total = total + AddGST(value);
                    Console.WriteLine($"Total is {total}");
                    Console.WriteLine("Please type in a number to add to total, and press enter");
                }

            } while (keyInfo.Key != ConsoleKey.Escape);                        
        }

        static double convertToDouble(string input)
        {
            var distance = 0.00;
            if (!double.TryParse(input, out distance))
            {               
                return distance;
            }
            return distance;
        }

        static double AddGST(double input)
        {
            var value = 0.00;
            var GST = 1.15;

            value = input * GST;
            value = System.Math.Round(value, 2);          
            return value;
        }
    }
}
