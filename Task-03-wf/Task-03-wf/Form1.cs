﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_03_wf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnCal_Click(object sender, EventArgs e)
        {
            var input = "";
            var input2 = "";

            var num = 0.00;
            var num2 = 0.00;

            var showAnswer = 0.00;


            input = txtInput.Text;
            input2 = txtInput2.Text;
            if (convertToDouble(input) == 0)
            {
                lblAnswer.Text = $"{input} is not vaild.  Please enter a number value !";
                return;
            }

            if (convertToDouble(input2) == 0)
            {
                lblAnswer.Text = $"{input2} is not vaild.  Please enter a number value !";
                return;
            }

            num = convertToDouble(input);
            num2 = convertToDouble(input2);

            if (radAdd.Checked)
            {
                showAnswer = add(num, num2);
                lblAnswer.Text = $"{input} + {input2} = {showAnswer}";
            }

            else if (radMinus.Checked)
            {
                showAnswer = minus(num, num2);
                lblAnswer.Text = ($"{input} - {input2} = {showAnswer}");
            }

        }

        static double add(double num1, double num2)
        {
            var answer = num1 + num2;
            answer = System.Math.Round(answer, 2);

            return answer;
        }

        static double minus(double num1, double num2)
        {
            var answer = num1 - num2;
            answer = System.Math.Round(answer, 2);

            return answer;
        }

        static double convertToDouble(string input)
        {
            var distance = 0.00;
            double.TryParse(input, out distance);

            return distance;
        }

        private void radMinus_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
