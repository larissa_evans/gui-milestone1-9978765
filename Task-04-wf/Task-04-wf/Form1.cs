﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_04_wf
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnGuess_Click(object sender, EventArgs e)
        {
            var input = "";

            var dictionary = createDictionary();
            input = txtGuess.Text;

            if (dictionary.ContainsKey(input.ToLower()))
            {
                lblMsg.Text = $"{input} is in my list of fruit and vege";
            }
            else
            {
                lblMsg.Text = $"Sorry {input} is not in my list of fruit and vege";
            }
        }

        static Dictionary<string, string> createDictionary()
        {
            var dictionary = new Dictionary<string, string>();
            dictionary.Add("apple", "fruit");
            dictionary.Add("orange", "fruit");
            dictionary.Add("banana", "fruit");
            dictionary.Add("peach", "fruit");
            dictionary.Add("cherry", "fruit");
            dictionary.Add("potatoe", "vege");
            dictionary.Add("lettuce", "vege");
            dictionary.Add("pumpkin", "vege");

            return dictionary;
        }
    }
}
