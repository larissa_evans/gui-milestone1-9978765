﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_05_console
{
    class Program
    {
        public static List<string> answers = new List<string>();
        static int tries = 0;
        static int score = 0;
        static void Main(string[] args)
        {
            var input = "";
            

            Console.WriteLine($"Im thinking of a number between 1 and 5. Take 5 guesses at what the number could be");
            input = Console.ReadLine();


            while (Checker(input) != 5)
            {
                Console.WriteLine($"Your score is {score}{Environment.NewLine}Take another guess");
                input = Console.ReadLine();               
            }

            Console.WriteLine($"Your guesses: ");

            foreach (string str in answers)
            {
                Console.WriteLine($"{Environment.NewLine}{str}");
            }
            return;
        }

        private static int Checker(string input)
        {
            Random rnd = new Random();
            var num = 0.00;
            int[] numbers = new int[] { 1, 2, 3, 4, 5 };

            tries = tries + 1;
            if (tries > 5)
            {
                return tries;
            }
            num = numbers[rnd.Next(numbers.Count())];
            if (input != num.ToString())
            {
                answers.Add($"{input} incorrect the correct number was {num}");
            }
            else
            {
                answers.Add($"WELL DONE! You guessed my number {num} :)");
                score = score + 1;
            }
            Console.WriteLine($"You have {5 - tries} guesses left");

            return tries;
        }
    }
}
