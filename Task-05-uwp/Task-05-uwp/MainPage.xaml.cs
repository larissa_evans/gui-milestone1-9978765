﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_05_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public static List<string> answers = new List<string>();
        static int tries = 0;
        static int score = 0;

        public MainPage()
        {
            this.InitializeComponent();
        }

        private void btnAgain_Click(object sender, RoutedEventArgs e)
        {
            var input = "";

            input = txtGuess.Text;
            lblScore.Text = $"Your score is {score}";

            while (Checker(input) == 5)
            {
                lblMsg.Text = $"Your guesses: ";

                foreach (string str in answers)
                {
                    lblMsg.Text += (Environment.NewLine + str);
                }
                btnAgain.Visibility = Visibility.Visible;
                btnGuess.Visibility = Visibility.Collapsed;
                return;
            }
        }

        private void btnGuess_Click(object sender, RoutedEventArgs e)
        {
            txtGuess.Text = "";
            answers.Clear();
            tries = 0;
            btnAgain.Visibility = Visibility.Collapsed;
            btnGuess.Visibility = Visibility.Visible;
            lblMsg.Text = "";
        }

        private int Checker(string input)
        {
            Random rnd = new Random();
            var num = 0.00;
            int[] numbers = new int[] { 1, 2, 3, 4, 5 };

            tries = tries + 1;
            if (tries > 5)
            {
                return tries;
            }
            num = numbers[rnd.Next(numbers.Count())];
            if (input != num.ToString())
            {
                answers.Add($"{input} incorrect the correct number was {num}");
            }
            else
            {
                answers.Add($"WELL DONE! You guessed my number {num} :)");
                score = score + 1;
            }
            lblMsg.Text = $"You have {5 - tries} guesses left";

            return tries;
        }
    }
}
