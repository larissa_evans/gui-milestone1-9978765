﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_01_wf
{
    public partial class Task1 : Form
    {
        public Task1()
        {
            InitializeComponent();
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            var Conversion = 0.00;
            var distance = convertToDouble(txtValue.Text);

            if(distance == 0)
            {
                lblAnswer.Text = "ERROR";
                return;
            }

            if(radKmtoMiles.Checked)
            {
                Conversion = convertToMiles(distance);
                lblAnswer.Text = $"{distance} KM is {Conversion} Miles";
            }
            else if (radMilestoKM.Checked)
            {
                Conversion = convertToKM(distance) ;
                lblAnswer.Text = $"{distance} Miles is {Conversion} KM";
            }

        }

        static double convertToDouble(string input)
        {
            var distance = 0.00;
            double.TryParse(input, out distance);
                        
            return distance;
        }

        static double convertToKM(double km)
        {
            const double km2miles = 0.621371192;
            var answer = km * km2miles;
            answer = System.Math.Round(answer, 2);
            return answer;
        }

        static double convertToMiles(double miles)
        {
            const double miles2km = 1.609344;
            var answer = miles * miles2km;
            answer = System.Math.Round(answer, 2);
            return answer;
        }
        private void txtValue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnConvert.PerformClick();
                e.SuppressKeyPress = true; // stop beep
                e.Handled = true;
            }

        }
    }
}
