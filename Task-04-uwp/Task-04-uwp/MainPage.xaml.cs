﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_04_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void btnGuess_Click(object sender, RoutedEventArgs e)
        {
            var input = "";

            var dictionary = createDictionary();
            input = txtGuess.Text;

            if (dictionary.ContainsKey(input.ToLower()))
            {
                lblMsg.Text = $"{input} is in my list of fruit and vege";
            }
            else
            {
                lblMsg.Text = $"Sorry {input} is not in my list of fruit and vege";
            }
        }

        static Dictionary<string, string> createDictionary()
        {
            var dictionary = new Dictionary<string, string>();
            dictionary.Add("apple", "fruit");
            dictionary.Add("orange", "fruit");
            dictionary.Add("banana", "fruit");
            dictionary.Add("peach", "fruit");
            dictionary.Add("cherry", "fruit");
            dictionary.Add("potatoe", "vege");
            dictionary.Add("lettuce", "vege");
            dictionary.Add("pumpkin", "vege");

            return dictionary;
        }
    }
}
