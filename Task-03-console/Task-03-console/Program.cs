﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_03_console
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = "";
            var input2 = "";

            var num = 0.00;
            var num2 = 0.00;

            var showAnswer = 0.00;

            string[] validChars = new string[] { "1", "2" };

            Console.WriteLine("Please press 1 for adding two numbers or press 2 for minusing two numbers");
            input = Console.ReadLine();

            while (!validChars.Contains(input))
            {
                Console.WriteLine($"{input} is not one of the options, please try again");
                input = Console.ReadLine();
            }

            if (input == "1")
            {
                Console.WriteLine("Please type in a number to add to");
                input = Console.ReadLine();

                while (convertToDouble(input) == 0)
                {
                    Console.WriteLine($"{input} is not vaild.  Please enter a number value !");
                    input = Console.ReadLine();
                }

                Console.WriteLine($"Please type in a number to add to {input}");
                input2 = Console.ReadLine();

                while (convertToDouble(input2) == 0)
                {
                    Console.WriteLine($"{input2} is not vaild.  Please enter a number value !");
                    input2 = Console.ReadLine();
                }

                num = convertToDouble(input);
                num2 = convertToDouble(input);
                showAnswer = add(num, num2);
                Console.WriteLine($"{input} + {input2} = {showAnswer}");
            }
            else if (input == "2")
            {
                Console.WriteLine("Please type in a number to minus from");
                input = Console.ReadLine();

                while (convertToDouble(input) == 0)
                {
                    Console.WriteLine($"{input} is not vaild.  Please enter a number value !");
                    input = Console.ReadLine();
                }

                Console.WriteLine($"Please type in a number to minus from {input}");
                input2 = Console.ReadLine();

                while (convertToDouble(input2) == 0)
                {
                    Console.WriteLine($"{input2} is not vaild.  Please enter a number value !");
                    input2 = Console.ReadLine();
                }

                num = convertToDouble(input);
                num2 = convertToDouble(input2);

                showAnswer = minus(num, num2);
                Console.WriteLine($"{input} - {input2} = {showAnswer}");
            }
            else
            {
                Console.WriteLine($"{input} is not one of the options, please try again");
                input = Console.ReadLine();
            }
        }

        static double add(double num1, double num2)
        {
            var answer = num1 + num2;
            answer = System.Math.Round(answer, 2);

            return answer;
        }

        static double minus(double num1, double num2)
        {
            var answer = num1 - num2;
            answer = System.Math.Round(answer, 2);

            return answer;
        }

        static double convertToDouble(string input)
        {
            var distance = 0.00;
            double.TryParse(input, out distance);

            return distance;
        }
    }
}
