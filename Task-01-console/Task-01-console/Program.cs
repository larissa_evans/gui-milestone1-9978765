﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_01_console
{
    class Program
    {
        static void Main(string[] args)
        {

            var input = "";
            var num = 0.00;
            var showConversion = 0.00;
            string[] validChars = new string[] { "1", "2" };

            Console.WriteLine("Please press 1 for KM to Miles or press 2 for Miles to KM");
            input = Console.ReadLine();

            while (!validChars.Contains(input))
            {
                Console.WriteLine($"{input} is not one of the options, please try again");
                input = Console.ReadLine();                
            }
                                                         
            if (input == "1")
            {
                Console.WriteLine("Please type in a number to convert KM to Miles");
                input = Console.ReadLine();

                while (convertToDouble(input) == 0)
                {
                    Console.WriteLine($"{input} is not vaild.  Please enter a number value !");
                    input = Console.ReadLine();
                }

                num = convertToDouble(input);
                showConversion = convertToKM(num);
                Console.WriteLine($"{input} KM is {showConversion} Miles");
            }
            else if(input == "2")
            {
                Console.WriteLine("Please type in a number to convert Miles to KM");
                input = Console.ReadLine();

                while (convertToDouble(input) == 0)
                {
                    Console.WriteLine($"{input} is not vaild.  Please enter a number value !");
                    input = Console.ReadLine();
                }

                num = convertToDouble(input);

                showConversion = convertToMiles(num);
                Console.WriteLine($"{input} Miles is {showConversion} KM");                
            }
                      
        }

        static double convertToKM(double km)
        {
            const double km2miles = 0.621371192;
            var answer = km * km2miles;
            answer = System.Math.Round(answer, 2);

            return answer;
        }

        static double convertToMiles(double miles)
        {
            const double miles2km = 1.609344;
            var answer = miles * miles2km;
            answer = System.Math.Round(answer, 2);

            return answer;
        }

        static double convertToDouble(string input)
        {
            var distance = 0.00;
            double.TryParse(input, out distance);
                        
            return distance;
        }

    }
}
