﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_02_wf
{
    public partial class Form1 : Form
    {
        double answer = 0.00;
        static double total = 0.00;
        public Form1()
        {
            InitializeComponent();
        }
        
        private void btnCalc_Click(object sender, EventArgs e)
        {
            answer = convertToDouble(txtValue.Text.Trim());
            if (answer > 0)
            {
                total = AddGST(answer + total);               
                lblTotal.Text = $"Total : {total}";                
                btnCalc.Text = "Add on";                
            }
        }

        static double convertToDouble(string input)
        {
            var distance = 0.00;
            if (!double.TryParse(input, out distance))
            {
                return distance;
            }
            return distance;
        }

        static double AddGST(double input)
        {
            var value = 0.00;
            var GST = 1.15;

            value = input * GST;
            value = System.Math.Round(value, 2);

            return value;
        }

        private void btnReset_Click(object sender, EventArgs e)
        {
            total = 0.00;
            lblTotal.ResetText();
            txtValue.ResetText();
        }
    }
}
