﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_03_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        
       private void btnCal_Click(object sender, RoutedEventArgs e)
        {
            var input = "";
            var input2 = "";

            var num = 0.00;
            var num2 = 0.00;

            var showAnswer = 0.00;


            input = txtInput.Text;
            input2 = txtInput2.Text;
            if (convertToDouble(input) == 0)
            {
                txtAnswer.Text = $"{input} is not vaild.  Please enter a number value !";
                return;
            }

            if (convertToDouble(input2) == 0)
            {
                txtAnswer.Text = $"{input2} is not vaild.  Please enter a number value !";
                return;
            }

            num = convertToDouble(input);
            num2 = convertToDouble(input2);

            if (radAdd.IsChecked == true)
            {
                showAnswer = add(num, num2);
                txtAnswer.Text = $"{input} + {input2} = {showAnswer}";
            }

            else if (radMinus.IsChecked == true)
            {
                showAnswer = minus(num, num2);
                txtAnswer.Text = ($"{input} - {input2} = {showAnswer}");
            }
        }

        static double add(double num1, double num2)
        {
            var answer = num1 + num2;
            answer = System.Math.Round(answer, 2);

            return answer;
        }

        static double minus(double num1, double num2)
        {
            var answer = num1 - num2;
            answer = System.Math.Round(answer, 2);

            return answer;
        }

        static double convertToDouble(string input)
        {
            var distance = 0.00;
            double.TryParse(input, out distance);

            return distance;
        }
    }
}
