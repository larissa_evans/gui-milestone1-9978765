﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_02_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        double answer = 0.00;
        static double total = 0.00;
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void btnCalc_Click(object sender, RoutedEventArgs e)
        {
            answer = convertToDouble(txtValue.Text.Trim());
            if (answer > 0)
            {
                total = AddGST(answer + total);
                lblTotal.Text = $"Total : {total}";
                btnCalc.Content = "Add on";
            }
        }

        static double convertToDouble(string input)
        {
            var distance = 0.00;
            if (!double.TryParse(input, out distance))
            {
                return distance;
            }
            return distance;
        }

        static double AddGST(double input)
        {
            var value = 0.00;
            var GST = 1.15;

            value = input * GST;
            value = System.Math.Round(value, 2);

            return value;
        }

      
    }
}
