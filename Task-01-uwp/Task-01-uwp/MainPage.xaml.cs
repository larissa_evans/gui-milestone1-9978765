﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Task_01_uwp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
        }

        private void btnConvert_Click(object sender, RoutedEventArgs e)
        {
            var Conversion = 0.00;
            var distance = convertToDouble(txtValue.Text);

            if (distance == 0)
            {
                lblAnswer.Text = "ERROR";
                return;
            }
                        
            if (radKMtoMiles.IsChecked == true)
            {
                Conversion = convertToMiles(distance);
                lblAnswer.Text = $"{distance} KM is {Conversion} Miles";
            }
            else if (radMilestoKM.IsChecked == true)
            {
                Conversion = convertToKM(distance);
                lblAnswer.Text = $"{distance} Miles is {Conversion} KM";
            }
        }
        
        static double convertToDouble(string input)
        {
            var distance = 0.00;
            double.TryParse(input, out distance);
            
            return distance;
        }

        static double convertToKM(double km)
        {
            const double km2miles = 0.621371192;
            var answer = km * km2miles;
            answer = System.Math.Round(answer, 2);
            return answer;
        }

        static double convertToMiles(double miles)
        {
            const double miles2km = 1.609344;
            var answer = miles * miles2km;
            answer = System.Math.Round(answer, 2);
            return answer;
        }
      
    }
    
}
