﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Task_05_wf
{
    public partial class Form1 : Form
    {
        public static List<string> answers = new List<string>();
        static int tries = 0;
        static int score = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void btnGuess_Click(object sender, EventArgs e)
        {
            var input = "";
           
            input = txtGuess.Text;
            lblScore.Text = $"Your score is {score}";

            while (Checker(input) == 5)
            {
                lblMsg.Text = $"Your guesses: ";

                foreach (string str in answers)
                {
                    lblMsg.Text += (Environment.NewLine + str);
                }
                btnAgain.Visible = true;
                btnGuess.Visible = false;
                return;
            }
            
        }

        private  int Checker(string input)
        {
            Random rnd = new Random();
            var num = 0.00;
            int[] numbers = new int[] { 1, 2, 3, 4, 5 };

            tries = tries + 1;
            if(tries > 5)
            {
                return tries;
            }
            num = numbers[rnd.Next(numbers.Count())];
            if (input != num.ToString())
            {
                answers.Add($"{input} incorrect the correct number was {num}");
            }
            else
            {
                answers.Add($"WELL DONE! You guessed my number {num} :)");
                score = score + 1;
            }
            lblMsg.Text = $"You have {5-tries} guesses left";
            
            return tries;
        }

        private void btnAgain_Click(object sender, EventArgs e)
        {
            txtGuess.Text = "";
            answers.Clear();
            tries = 0;
            btnGuess.Visible = true;
            btnAgain.Visible = false;
            lblMsg.Text = "";
        }
    }
}
