﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task_04_console
{
    class Program
    {
        static void Main(string[] args)
        {
            var input = "";

            var dictionary = createDictionary();
            Console.WriteLine("Guess a fruit or vege that could be in my list");

            input = Console.ReadLine();

            if (dictionary.ContainsKey(input.ToLower()))
            {
                Console.WriteLine($"{input} is in my list of fruit and vege");
            }
            else
            {
                Console.WriteLine($"Sorry {input} is not in my list of fruit and vege");
            }
        }

        static Dictionary<string, string> createDictionary()
        {
            var dictionary = new Dictionary<string, string>();
            dictionary.Add("apple", "fruit");
            dictionary.Add("orange", "fruit");
            dictionary.Add("banana", "fruit");
            dictionary.Add("peach", "fruit");
            dictionary.Add("cherry", "fruit");
            dictionary.Add("potatoe", "vege");
            dictionary.Add("lettuce", "vege");
            dictionary.Add("pumpkin", "vege");

            return dictionary;
        }

    }
}
